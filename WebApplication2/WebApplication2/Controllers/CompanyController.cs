﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebApplication2.Controllers;
using WebApplication2.Entities;
using WebApplication2.Models;

namespace Ummc.Web.Controllers
{
    public class CompanyController : HomeController
    {
        public ActionResult Index()
        {
            List<CompanyModel> CompanyModels = new List<CompanyModel>();

            var entities = Repository.Companies.ToList();

            foreach (var entity in entities)
            {
                CompanyModels.Add(new CompanyModel
                {
                    Id = entity.Id,
                    Name = entity.Name
                });
            }

            return View(CompanyModels);
        }
        public ActionResult Create()
        {
            ViewBag.Companies = new SelectList(Repository.Companies, "Id", "Name");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CompanyModel model)
        {

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            Company entity = new Company(model.Name);

            Repository.Companies.Add(entity);
            Repository.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var entity = Repository.Companies.Find(id);
            var company = Repository.Companies.ToList().SingleOrDefault(x => x.Id == entity.Id);
            ViewBag.Companies = new SelectList(Repository.Companies, "Id", "Name", company.Id);
            if (entity != null)
            {

                var viewmodel = new CompanyModel
                {
                    Name = entity.Name,

                };
                return View(viewmodel);
            }
            return RedirectToAction("Index");

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CompanyModel model)
        {
            var entity = Repository.Companies.Find(model.Id);
            var room = Repository.Companies.SingleOrDefault(x => x.Id == model.Id);
            if (entity != null)
            {
                entity.Update(model.Name);
                Repository.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            var entity = Repository.Companies.Find(id);
            if (entity != null)
            {
                Repository.Companies.Remove(entity);
                Repository.SaveChanges();
            }
            return RedirectToAction("Index");

        }
    }
}