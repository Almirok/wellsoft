﻿using System.Web.Mvc;
using WebApplication2.Data;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        public ApplicationDbContext Repository { get; } = new ApplicationDbContext();
    }
}