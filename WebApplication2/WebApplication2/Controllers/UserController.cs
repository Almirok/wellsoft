﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebApplication2.Controllers;
using WebApplication2.Entities;
using WebApplication2.Models;

namespace Ummc.Web.Controllers
{
    public class UserController : HomeController
    {
        public ActionResult Index()
        {
            List<UserModel> UserModels = new List<UserModel>();

            var entities = Repository.Users.ToList();
            var companies = Repository.Companies.ToList();
            foreach (var entity in entities)
            {
                var company = companies.SingleOrDefault(x => x.Id == entity.Company.Id);
                UserModels.Add(new UserModel
                {
                    Id = entity.Id,
                    Surname = entity.Surname,
                    Name = entity.Name,
                    Lastname = entity.Lastname,
                    Number = entity.Number,
                    Company = new CompanyModel { Id = entity.Company.Id, Name = entity.Company.Name },
                });
            }

            return View(UserModels);
        }
        public ActionResult Create()
        {
            ViewBag.Companies = new SelectList(Repository.Companies, "Id", "Name");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserModel model)
        {
            var company = Repository.Companies.SingleOrDefault(x => x.Id == model.Company.Id);

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            User entity = new User(model.Surname, model.Name, model.Lastname, model.Number, company);

            Repository.Users.Add(entity);
            Repository.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var entity = Repository.Users.Find(id);
            var company = Repository.Companies.ToList().SingleOrDefault(x => x.Id == entity.Company.Id);
            ViewBag.Companies = new SelectList(Repository.Companies, "Id", "Name", company.Id);
            if (entity != null)
            {

                var viewmodel = new UserModel
                {
                    Id = entity.Id,
                    Surname = entity.Surname,
                    Name = entity.Name,
                    Lastname = entity.Lastname,
                    Number = entity.Number,
                    Company = new CompanyModel { Id = company.Id, Name = company.Name },

                };
                return View(viewmodel);
            }
            return RedirectToAction("Index");

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserModel model)
        {
            var entity = Repository.Users.Find(model.Id);
            var room = Repository.Companies.SingleOrDefault(x => x.Id == model.Company.Id);
            if (entity != null)
            {
                entity.Update(model.Surname, model.Name, model.Lastname, model.Number, room);
                Repository.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            var entity = Repository.Users.Find(id);
            if (entity != null)
            {
                Repository.Users.Remove(entity);
                Repository.SaveChanges();
            }
            return RedirectToAction("Index");

        }
    }
}