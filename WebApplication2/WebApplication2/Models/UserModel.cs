﻿namespace WebApplication2.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Lastname { get; set; }
        public string Number { get; set; }
        public virtual CompanyModel Company { get; set; }
    }
}