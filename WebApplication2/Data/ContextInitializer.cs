﻿using System.Data.Entity;

namespace WebApplication2.Data
{
    public class ContextInitializer
    : MigrateDatabaseToLatestVersion<ApplicationDbContext, Migrations.Configuration>
    {

    }
}