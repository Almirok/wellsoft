﻿using WebApplication2.Interfaces;

namespace Ummc.Domain.Core.Entities
{
    public class IdentifiableEntity<TKey> : IIdentifiableEntity<TKey> //автоподстановка id в сущности
    {
        public virtual TKey Id { get; protected set; }
    }
}
