﻿using Ummc.Domain.Core.Entities;

namespace WebApplication2.Entities
{
    public class User : IdentifiableEntity<int>
    {
        protected User()
        { }
        public string Surname { get; protected set; }
        public string Name { get; protected set; } 
        public string Lastname { get; protected set; }
        public string Number { get; protected set; }
        public Company Company { get; protected set; } 

        public User(string surname, string name, string lastname, string number, Company company)
        {
            Surname = surname;
            Name = name;
            Lastname = lastname;
            Number = number;
            Company = company;
        }

        public User Update(string surname, string name, string lastname, string number, Company company)
        {
            Surname = surname;
            Name = name;
            Lastname = lastname;
            Number = number;
            Company = company;

            return this;
        }
    }
}