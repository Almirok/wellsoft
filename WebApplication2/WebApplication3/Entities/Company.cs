﻿using Ummc.Domain.Core.Entities;


namespace WebApplication2.Entities
{
    public class Company : IdentifiableEntity<int>
    {
        protected Company()
        { }

        public string Name { get; protected set; } 

        public Company( string name)
        {
            Name = name;
        }

        public Company Update( string name)
        {
            Name = name;

            return this;
        }
    }
}