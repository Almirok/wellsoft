﻿namespace WebApplication2.Interfaces
{
    public interface IIdentifiableEntity<out TKey>
    {
        TKey Id { get; }
    }
}
